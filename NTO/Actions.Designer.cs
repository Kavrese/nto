﻿namespace NTO
{
    partial class Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dgvBooking = new DataGridView();
            DataStart = new DataGridViewTextBoxColumn();
            DataEnd = new DataGridViewTextBoxColumn();
            FIO = new DataGridViewTextBoxColumn();
            IsCompleteEnter = new DataGridViewCheckBoxColumn();
            IsCompleExit = new DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dgvBooking).BeginInit();
            SuspendLayout();
            // 
            // dgvBooking
            // 
            dgvBooking.AllowUserToAddRows = false;
            dgvBooking.AllowUserToDeleteRows = false;
            dgvBooking.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvBooking.Columns.AddRange(new DataGridViewColumn[] { DataStart, DataEnd, FIO, IsCompleteEnter, IsCompleExit });
            dgvBooking.Location = new Point(12, 12);
            dgvBooking.Name = "dgvBooking";
            dgvBooking.ReadOnly = true;
            dgvBooking.RowTemplate.Height = 25;
            dgvBooking.Size = new Size(709, 342);
            dgvBooking.TabIndex = 1;
            // 
            // DataStart
            // 
            DataStart.DataPropertyName = "dateStart";
            DataStart.HeaderText = "Дата заезда";
            DataStart.Name = "DataStart";
            DataStart.ReadOnly = true;
            // 
            // DataEnd
            // 
            DataEnd.DataPropertyName = "dateEnd";
            DataEnd.HeaderText = "Дата выезда";
            DataEnd.Name = "DataEnd";
            DataEnd.ReadOnly = true;
            // 
            // FIO
            // 
            FIO.DataPropertyName = "FIO";
            FIO.HeaderText = "Клиент";
            FIO.Name = "FIO";
            FIO.ReadOnly = true;
            // 
            // IsCompleteEnter
            // 
            IsCompleteEnter.DataPropertyName = "isCompleteEnter";
            IsCompleteEnter.HeaderText = "Гость заехал";
            IsCompleteEnter.Name = "IsCompleteEnter";
            IsCompleteEnter.ReadOnly = true;
            // 
            // IsCompleExit
            // 
            IsCompleExit.DataPropertyName = "isCompleteExit";
            IsCompleExit.HeaderText = "Гость уехал";
            IsCompleExit.Name = "IsCompleExit";
            IsCompleExit.ReadOnly = true;
            IsCompleExit.Resizable = DataGridViewTriState.True;
            IsCompleExit.SortMode = DataGridViewColumnSortMode.Automatic;
            // 
            // Actions
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(734, 367);
            Controls.Add(dgvBooking);
            Name = "Actions";
            ((System.ComponentModel.ISupportInitialize)dgvBooking).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvBooking;
        private DataGridViewTextBoxColumn DataStart;
        private DataGridViewTextBoxColumn DataEnd;
        private DataGridViewTextBoxColumn FIO;
        private DataGridViewCheckBoxColumn IsCompleteEnter;
        private DataGridViewCheckBoxColumn IsCompleExit;
    }
}