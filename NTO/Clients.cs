﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class Clients : Form
    {
        public Clients()
        {
            InitializeComponent();
        }

        List<ModelClient> clients;

        private void Clients_Load(object sender, EventArgs e)
        {
            dgvClients.AutoGenerateColumns = false;
            using (Context context = new())
            {
                clients = context.GetClients();
                dgvClients.DataSource = clients;
            }
        }

        private void btnClients_Click(object sender, EventArgs e)
        {
            var form = new AddClients();
            form.Closed += Closed;
            form.Show();
        }

        public void Closed(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                clients = context.GetClients();
                dgvClients.DataSource = clients;
            }
        }
    }
}
