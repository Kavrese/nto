﻿namespace NTO
{
    partial class AddClients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddClients));
            cbClients = new ComboBox();
            btnAddClients = new Button();
            checkBoxYrFace = new CheckBox();
            SuspendLayout();
            // 
            // cbClients
            // 
            cbClients.DisplayMember = "FIO";
            cbClients.FormattingEnabled = true;
            cbClients.Location = new Point(10, 10);
            cbClients.Name = "cbClients";
            cbClients.Size = new Size(632, 23);
            cbClients.TabIndex = 0;
            // 
            // btnAddClients
            // 
            btnAddClients.BackColor = SystemColors.ActiveCaption;
            btnAddClients.Location = new Point(477, 37);
            btnAddClients.Name = "btnAddClients";
            btnAddClients.Size = new Size(165, 36);
            btnAddClients.TabIndex = 1;
            btnAddClients.Text = "Добавить";
            btnAddClients.UseVisualStyleBackColor = false;
            btnAddClients.Click += btnAddClients_Click;
            // 
            // checkBoxYrFace
            // 
            checkBoxYrFace.AutoSize = true;
            checkBoxYrFace.BackColor = SystemColors.ControlLight;
            checkBoxYrFace.Location = new Point(10, 36);
            checkBoxYrFace.Margin = new Padding(3, 2, 3, 2);
            checkBoxYrFace.Name = "checkBoxYrFace";
            checkBoxYrFace.Size = new Size(79, 19);
            checkBoxYrFace.TabIndex = 2;
            checkBoxYrFace.Text = "Юр. лицо";
            checkBoxYrFace.UseVisualStyleBackColor = false;
            // 
            // AddClients
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(650, 84);
            Controls.Add(checkBoxYrFace);
            Controls.Add(btnAddClients);
            Controls.Add(cbClients);
            Name = "AddClients";
            Text = "Добавить клиента";
            Load += AddClients_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbClients;
        private Button btnAddClients;
        private CheckBox checkBoxYrFace;
    }
}