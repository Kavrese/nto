﻿namespace NTO
{
    partial class Room
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Room));
            dgvRoom = new DataGridView();
            Number = new DataGridViewTextBoxColumn();
            Category = new DataGridViewTextBoxColumn();
            CountHumans = new DataGridViewTextBoxColumn();
            Status = new DataGridViewTextBoxColumn();
            buttonRoom = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvRoom).BeginInit();
            SuspendLayout();
            // 
            // dgvRoom
            // 
            dgvRoom.AllowUserToAddRows = false;
            dgvRoom.AllowUserToDeleteRows = false;
            dgvRoom.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvRoom.Columns.AddRange(new DataGridViewColumn[] { Number, Category, CountHumans, Status });
            dgvRoom.Location = new Point(3, 12);
            dgvRoom.Name = "dgvRoom";
            dgvRoom.ReadOnly = true;
            dgvRoom.RowHeadersVisible = false;
            dgvRoom.RowHeadersWidth = 51;
            dgvRoom.RowTemplate.Height = 25;
            dgvRoom.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvRoom.Size = new Size(738, 286);
            dgvRoom.TabIndex = 0;
            // 
            // Number
            // 
            Number.DataPropertyName = "Number";
            Number.HeaderText = "Номер";
            Number.MinimumWidth = 6;
            Number.Name = "Number";
            Number.ReadOnly = true;
            Number.Width = 125;
            // 
            // Category
            // 
            Category.DataPropertyName = "CategoryName";
            Category.HeaderText = "Категория";
            Category.MinimumWidth = 6;
            Category.Name = "Category";
            Category.ReadOnly = true;
            Category.Width = 125;
            // 
            // CountHumans
            // 
            CountHumans.DataPropertyName = "CountHumans";
            CountHumans.HeaderText = "Количество мест";
            CountHumans.MinimumWidth = 6;
            CountHumans.Name = "CountHumans";
            CountHumans.ReadOnly = true;
            CountHumans.Width = 125;
            // 
            // Status
            // 
            Status.DataPropertyName = "StatusName";
            Status.HeaderText = "Статус";
            Status.MinimumWidth = 6;
            Status.Name = "Status";
            Status.ReadOnly = true;
            Status.Width = 125;
            // 
            // buttonRoom
            // 
            buttonRoom.BackColor = SystemColors.ActiveCaption;
            buttonRoom.Location = new Point(591, 304);
            buttonRoom.Name = "buttonRoom";
            buttonRoom.Size = new Size(150, 38);
            buttonRoom.TabIndex = 1;
            buttonRoom.Text = "Добавить комнату";
            buttonRoom.UseVisualStyleBackColor = false;
            buttonRoom.Click += buttonRoom_Click;
            // 
            // Room
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(751, 369);
            Controls.Add(buttonRoom);
            Controls.Add(dgvRoom);
            Name = "Room";
            Text = "Номера";
            Load += Room_Load;
            ((System.ComponentModel.ISupportInitialize)dgvRoom).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvRoom;
        private Button buttonRoom;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn Category;
        private DataGridViewTextBoxColumn CountHumans;
        private DataGridViewTextBoxColumn Status;
    }
}