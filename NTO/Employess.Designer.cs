﻿namespace NTO
{
    partial class Employess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dgvEmp = new DataGridView();
            LastName = new DataGridViewTextBoxColumn();
            FirstName = new DataGridViewTextBoxColumn();
            PatronymicName = new DataGridViewTextBoxColumn();
            Category = new DataGridViewTextBoxColumn();
            lblEmp = new Label();
            button1 = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvEmp).BeginInit();
            SuspendLayout();
            // 
            // dgvEmp
            // 
            dgvEmp.AllowUserToAddRows = false;
            dgvEmp.AllowUserToDeleteRows = false;
            dgvEmp.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvEmp.Columns.AddRange(new DataGridViewColumn[] { LastName, FirstName, PatronymicName, Category });
            dgvEmp.Location = new Point(21, 49);
            dgvEmp.Name = "dgvEmp";
            dgvEmp.ReadOnly = true;
            dgvEmp.RowHeadersVisible = false;
            dgvEmp.RowTemplate.Height = 25;
            dgvEmp.Size = new Size(746, 354);
            dgvEmp.TabIndex = 0;
            // 
            // LastName
            // 
            LastName.DataPropertyName = "Lastname";
            LastName.HeaderText = "Фамилия";
            LastName.Name = "LastName";
            LastName.ReadOnly = true;
            LastName.Width = 150;
            // 
            // FirstName
            // 
            FirstName.DataPropertyName = "Firstname";
            FirstName.HeaderText = "Имя";
            FirstName.Name = "FirstName";
            FirstName.ReadOnly = true;
            FirstName.Width = 150;
            // 
            // PatronymicName
            // 
            PatronymicName.DataPropertyName = "Patronymic";
            PatronymicName.HeaderText = "Отчество";
            PatronymicName.Name = "PatronymicName";
            PatronymicName.ReadOnly = true;
            PatronymicName.Width = 150;
            // 
            // Category
            // 
            Category.DataPropertyName = "CategoryName";
            Category.HeaderText = "Категория";
            Category.Name = "Category";
            Category.ReadOnly = true;
            Category.Width = 200;
            // 
            // lblEmp
            // 
            lblEmp.AutoSize = true;
            lblEmp.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblEmp.Location = new Point(308, 9);
            lblEmp.Name = "lblEmp";
            lblEmp.Size = new Size(146, 32);
            lblEmp.TabIndex = 1;
            lblEmp.Text = "Сотрудники";
            // 
            // button1
            // 
            button1.Location = new Point(606, 415);
            button1.Name = "button1";
            button1.Size = new Size(161, 42);
            button1.TabIndex = 2;
            button1.Text = "Добавить сотрудника";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // Employess
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 469);
            Controls.Add(button1);
            Controls.Add(lblEmp);
            Controls.Add(dgvEmp);
            Name = "Employess";
            Text = "Сотрудники";
            Load += Employess_Load;
            ((System.ComponentModel.ISupportInitialize)dgvEmp).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView dgvEmp;
        private Label lblEmp;
        private DataGridViewTextBoxColumn LastName;
        private DataGridViewTextBoxColumn FirstName;
        private DataGridViewTextBoxColumn PatronymicName;
        private DataGridViewTextBoxColumn Category;
        private Button button1;
    }
}