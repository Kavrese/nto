﻿using NTO.database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTO
{
    public partial class RequestForCleaning : Form
    {
        public RequestForCleaning()
        {
            InitializeComponent();
        }

        private void btnRegClean_Click(object sender, EventArgs e)
        {
            Cleaning form = new();
            form.Closed += Closed;
            form.ShowDialog();
        }

        private void RequestForCleaning_Load(object sender, EventArgs e)
        {
            dgvRegClean.AutoGenerateColumns = false;
            using (Context context = new())
            {
                dgvRegClean.DataSource = context.GetCleanActions();
            }
        }

        public void Closed(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                dgvRegClean.DataSource = context.GetCleanActions();
            }
        }
    }
}
