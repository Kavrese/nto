﻿namespace NTO
{
    partial class Hotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dgvHotel = new DataGridView();
            Name = new DataGridViewTextBoxColumn();
            Region = new DataGridViewTextBoxColumn();
            btnRoom = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvHotel).BeginInit();
            SuspendLayout();
            // 
            // dgvHotel
            // 
            dgvHotel.AllowUserToAddRows = false;
            dgvHotel.AllowUserToDeleteRows = false;
            dgvHotel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvHotel.Columns.AddRange(new DataGridViewColumn[] { Name, Region });
            dgvHotel.Location = new Point(10, 10);
            dgvHotel.Name = "dgvHotel";
            dgvHotel.ReadOnly = true;
            dgvHotel.RowHeadersWidth = 51;
            dgvHotel.RowTemplate.Height = 25;
            dgvHotel.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvHotel.Size = new Size(734, 320);
            dgvHotel.TabIndex = 0;
            // 
            // Name
            // 
            Name.DataPropertyName = "Name";
            Name.HeaderText = "Название отеля";
            Name.MinimumWidth = 6;
            Name.Name = "Name";
            Name.ReadOnly = true;
            Name.Width = 400;
            // 
            // Region
            // 
            Region.DataPropertyName = "NameRegion";
            Region.HeaderText = "Регион";
            Region.MinimumWidth = 6;
            Region.Name = "Region";
            Region.ReadOnly = true;
            Region.Width = 125;
            // 
            // btnRoom
            // 
            btnRoom.BackColor = SystemColors.ActiveCaption;
            btnRoom.BackgroundImageLayout = ImageLayout.Stretch;
            btnRoom.Location = new Point(10, 336);
            btnRoom.Name = "btnRoom";
            btnRoom.Size = new Size(139, 36);
            btnRoom.TabIndex = 1;
            btnRoom.Text = "Номера отеля";
            btnRoom.UseVisualStyleBackColor = false;
            btnRoom.Click += btnRoom_Click;
            // 
            // Hotel
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = Properties.Resources.форма_заявки_на_бронирование_номера_в_гостинице_и_фон_кольцевой_темной_161175863;
            ClientSize = new Size(757, 388);
            Controls.Add(btnRoom);
            Controls.Add(dgvHotel);
            Text = "Отели";
            Load += Hotel_Load;
            ((System.ComponentModel.ISupportInitialize)dgvHotel).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvHotel;
        private Button btnRoom;
        private DataGridViewTextBoxColumn Name;
        private DataGridViewTextBoxColumn Region;
    }
}