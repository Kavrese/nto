﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class AddBooking : Form
    {
        public AddBooking()
        {
            InitializeComponent();
        }

        public bool isFirst = true;

        private void AddBooking_Load(object sender, EventArgs e)
        {
            dateTimePickerEnd.MinDate = dateTimePickerStart.Value.AddDays(1);
            using (Context context = new())
            {
                comboBoxClients.DataSource = context.Clients
                    .Include(t => t.Guest)
                    .ToList();

                comboBoxHotels.DataSource = context.GetHotels();

                var rooms = context.Rooms
                    .Include(t => t.Hotel)
                    .Include(t => t.Category)
                    .Include(t => t.Status)
                    .ToList();
                comboBoxRooms.DataSource = rooms;
            }
            isFirst = false;
            CalcSum();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                ModelBooking modelBooking = new ModelBooking();
                modelBooking.startDate = DateOnly.FromDateTime(dateTimePickerStart.Value);
                modelBooking.endDate = DateOnly.FromDateTime(dateTimePickerEnd.Value);

                modelBooking.RoomId = (comboBoxRooms.SelectedItem as ModelRoom).Id;
                modelBooking.HotelId = (comboBoxHotels.SelectedValue as ModelHotel).Id;
                modelBooking.ClientId = (comboBoxClients.SelectedValue as ModelClient).Id;

                context.Add(modelBooking);
                context.SaveChanges();
                MessageBox.Show("Сохранено");
                Close();
            }
        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerEnd.MinDate = dateTimePickerStart.Value.AddDays(1);
            CalcSum();
        }

        private void comboBoxRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcSum();
        }

        public void CalcSum()
        {
            if (isFirst) { return; }
            var hotel = comboBoxHotels.SelectedItem as ModelHotel;
            var room = comboBoxRooms.SelectedItem as ModelRoom;
            int cost = room.Category.Price * ((DateOnly.FromDateTime(dateTimePickerEnd.Value).ToDateTime(TimeOnly.MinValue) - DateOnly.FromDateTime(dateTimePickerStart.Value).ToDateTime(TimeOnly.MinValue)).Days);
            txtCost.Text = "Стоимость: " + cost.ToString();
            txtSale.Text = "Скидка: " + hotel.CategoryHotel.Sale.ToString() + "%";
            float sum = cost - cost * hotel.CategoryHotel.Sale / 100f;
            txtSum.Text = "Итого: " + string.Format("{0:0.00}", sum);
        }

        private void comboBoxHotels_SelectedIndexChanged(object sender, EventArgs e)
        {
            //using (Context context = new())
            //{
            //    var rooms = context.Rooms
            //                .Include(t => t.Hotel)
            //                .Include(t => t.Category)
            //                .Include(t => t.Status)
            //                .Where(t => t.HotelId == (comboBoxHotels.SelectedItem as ModelHotel).Id)
            //                .ToList();
            //    comboBoxRooms.DataSource = rooms;
            //}
        }

        private void dateTimePickerEnd_ValueChanged(object sender, EventArgs e)
        {
            CalcSum();
        }
    }
}
