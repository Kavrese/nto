﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class DHotels : Form
    {
        public DHotels()
        {
            InitializeComponent();
        }

        public List<ModelHotel> Hotels;

        private void DHotels_Load(object sender, EventArgs e)
        {
            dgvDHotels.AutoGenerateColumns = false;
            using (Context context = new())
            {
                Hotels = context.GetHotels();
                dgvDHotels.DataSource = Hotels;
            }
        }

        private void btnDHotels_Click(object sender, EventArgs e)
        {
            var hotel = Hotels.Where(t => t.Name == (string)dgvDHotels.SelectedRows[0].Cells[0].Value).ToList().Single();
            var form = new EditDHotels(hotel);
            form.Closed += Closed;
            form.Show();
        }

        public void Closed(object sender, EventArgs e)
        {
            using (Context context = new())
            {

                dgvDHotels.DataSource = context.GetHotels();
            }
        }
    }
}
