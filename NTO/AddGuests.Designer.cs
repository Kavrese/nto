﻿namespace NTO
{
    partial class AddGuests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddGuests));
            tbGLN = new TextBox();
            tbGFN = new TextBox();
            tbGPN = new TextBox();
            mtbGPhone = new MaskedTextBox();
            lblLN = new Label();
            lblFN = new Label();
            lblPN = new Label();
            lblPhone = new Label();
            btnAddGuests = new Button();
            SuspendLayout();
            // 
            // tbGLN
            // 
            tbGLN.Location = new Point(10, 27);
            tbGLN.Name = "tbGLN";
            tbGLN.Size = new Size(254, 23);
            tbGLN.TabIndex = 0;
            // 
            // tbGFN
            // 
            tbGFN.Location = new Point(11, 77);
            tbGFN.Name = "tbGFN";
            tbGFN.Size = new Size(253, 23);
            tbGFN.TabIndex = 1;
            // 
            // tbGPN
            // 
            tbGPN.Location = new Point(10, 123);
            tbGPN.Name = "tbGPN";
            tbGPN.Size = new Size(254, 23);
            tbGPN.TabIndex = 2;
            // 
            // mtbGPhone
            // 
            mtbGPhone.Location = new Point(10, 171);
            mtbGPhone.Mask = "+7 (999) 000-0000";
            mtbGPhone.Name = "mtbGPhone";
            mtbGPhone.Size = new Size(254, 23);
            mtbGPhone.TabIndex = 3;
            // 
            // lblLN
            // 
            lblLN.AutoSize = true;
            lblLN.BackColor = SystemColors.ControlLight;
            lblLN.Location = new Point(10, 9);
            lblLN.Name = "lblLN";
            lblLN.Size = new Size(58, 15);
            lblLN.TabIndex = 4;
            lblLN.Text = "Фамилия";
            // 
            // lblFN
            // 
            lblFN.AutoSize = true;
            lblFN.BackColor = SystemColors.ControlLight;
            lblFN.Location = new Point(10, 59);
            lblFN.Name = "lblFN";
            lblFN.Size = new Size(31, 15);
            lblFN.TabIndex = 5;
            lblFN.Text = "Имя";
            // 
            // lblPN
            // 
            lblPN.AutoSize = true;
            lblPN.BackColor = SystemColors.ControlLight;
            lblPN.Location = new Point(10, 104);
            lblPN.Name = "lblPN";
            lblPN.Size = new Size(58, 15);
            lblPN.TabIndex = 6;
            lblPN.Text = "Отчество";
            // 
            // lblPhone
            // 
            lblPhone.AutoSize = true;
            lblPhone.BackColor = SystemColors.ControlLight;
            lblPhone.Location = new Point(10, 153);
            lblPhone.Name = "lblPhone";
            lblPhone.Size = new Size(101, 15);
            lblPhone.TabIndex = 7;
            lblPhone.Text = "Номер телефона";
            // 
            // btnAddGuests
            // 
            btnAddGuests.BackColor = SystemColors.ActiveCaption;
            btnAddGuests.Location = new Point(10, 208);
            btnAddGuests.Name = "btnAddGuests";
            btnAddGuests.Size = new Size(254, 39);
            btnAddGuests.TabIndex = 8;
            btnAddGuests.Text = "Сохранить";
            btnAddGuests.UseVisualStyleBackColor = false;
            btnAddGuests.Click += btnAddGuests_Click;
            // 
            // AddGuests
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(276, 256);
            Controls.Add(btnAddGuests);
            Controls.Add(lblPhone);
            Controls.Add(lblPN);
            Controls.Add(lblFN);
            Controls.Add(lblLN);
            Controls.Add(mtbGPhone);
            Controls.Add(tbGPN);
            Controls.Add(tbGFN);
            Controls.Add(tbGLN);
            Name = "AddGuests";
            Text = "Добавить гостя";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbGLN;
        private TextBox tbGFN;
        private TextBox tbGPN;
        private MaskedTextBox mtbGPhone;
        private Label lblLN;
        private Label lblFN;
        private Label lblPN;
        private Label lblPhone;
        private Button btnAddGuests;
    }
}