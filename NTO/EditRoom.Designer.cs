﻿namespace NTO
{
    partial class EditRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRoom));
            tbEdRoom = new TextBox();
            cbEdCategory = new ComboBox();
            cbEdStatus = new ComboBox();
            btnEdRoom = new Button();
            lblRoom = new Label();
            lblPeople = new Label();
            lblCategory = new Label();
            lblStatus = new Label();
            cbHotel = new ComboBox();
            lblHotel = new Label();
            countHumans = new NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)countHumans).BeginInit();
            SuspendLayout();
            // 
            // tbEdRoom
            // 
            tbEdRoom.Location = new Point(232, 129);
            tbEdRoom.Name = "tbEdRoom";
            tbEdRoom.Size = new Size(201, 23);
            tbEdRoom.TabIndex = 0;
            // 
            // cbEdCategory
            // 
            cbEdCategory.DisplayMember = "Name";
            cbEdCategory.FormattingEnabled = true;
            cbEdCategory.Location = new Point(10, 79);
            cbEdCategory.Name = "cbEdCategory";
            cbEdCategory.Size = new Size(202, 23);
            cbEdCategory.TabIndex = 1;
            cbEdCategory.SelectedIndexChanged += cbEdCategory_SelectedIndexChanged;
            cbEdCategory.ValueMemberChanged += cbEdCategory_ValueMemberChanged;
            // 
            // cbEdStatus
            // 
            cbEdStatus.DisplayMember = "Name";
            cbEdStatus.FormattingEnabled = true;
            cbEdStatus.Location = new Point(231, 79);
            cbEdStatus.Name = "cbEdStatus";
            cbEdStatus.Size = new Size(202, 23);
            cbEdStatus.TabIndex = 3;
            // 
            // btnEdRoom
            // 
            btnEdRoom.BackColor = SystemColors.ActiveCaption;
            btnEdRoom.Location = new Point(244, 172);
            btnEdRoom.Name = "btnEdRoom";
            btnEdRoom.Size = new Size(188, 40);
            btnEdRoom.TabIndex = 4;
            btnEdRoom.Text = "Сохранить изменения";
            btnEdRoom.UseVisualStyleBackColor = false;
            btnEdRoom.Click += btnEdRoom_Click;
            // 
            // lblRoom
            // 
            lblRoom.AutoSize = true;
            lblRoom.BackColor = SystemColors.ActiveCaption;
            lblRoom.Location = new Point(232, 110);
            lblRoom.Name = "lblRoom";
            lblRoom.Size = new Size(45, 15);
            lblRoom.TabIndex = 5;
            lblRoom.Text = "Номер";
            // 
            // lblPeople
            // 
            lblPeople.AutoSize = true;
            lblPeople.BackColor = SystemColors.ActiveCaption;
            lblPeople.Location = new Point(10, 111);
            lblPeople.Name = "lblPeople";
            lblPeople.Size = new Size(144, 15);
            lblPeople.TabIndex = 6;
            lblPeople.Text = "Количество посетителей";
            // 
            // lblCategory
            // 
            lblCategory.AutoSize = true;
            lblCategory.BackColor = SystemColors.ActiveCaption;
            lblCategory.Location = new Point(10, 61);
            lblCategory.Name = "lblCategory";
            lblCategory.Size = new Size(108, 15);
            lblCategory.TabIndex = 7;
            lblCategory.Text = "Категория номера";
            // 
            // lblStatus
            // 
            lblStatus.AutoSize = true;
            lblStatus.BackColor = SystemColors.ActiveCaption;
            lblStatus.Location = new Point(231, 61);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new Size(88, 15);
            lblStatus.TabIndex = 8;
            lblStatus.Text = "Статус номера";
            // 
            // cbHotel
            // 
            cbHotel.DisplayMember = "Name";
            cbHotel.FormattingEnabled = true;
            cbHotel.Location = new Point(10, 32);
            cbHotel.Name = "cbHotel";
            cbHotel.Size = new Size(422, 23);
            cbHotel.TabIndex = 9;
            // 
            // lblHotel
            // 
            lblHotel.AutoSize = true;
            lblHotel.BackColor = SystemColors.ActiveCaption;
            lblHotel.Location = new Point(10, 7);
            lblHotel.Name = "lblHotel";
            lblHotel.Size = new Size(40, 15);
            lblHotel.TabIndex = 10;
            lblHotel.Text = "Отель";
            // 
            // countHumans
            // 
            countHumans.Location = new Point(10, 130);
            countHumans.Margin = new Padding(3, 2, 3, 2);
            countHumans.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            countHumans.Name = "countHumans";
            countHumans.Size = new Size(201, 23);
            countHumans.TabIndex = 11;
            countHumans.Value = new decimal(new int[] { 1, 0, 0, 0 });
            countHumans.ValueChanged += countHumans_ValueChanged;
            // 
            // EditRoom
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(454, 222);
            Controls.Add(countHumans);
            Controls.Add(lblHotel);
            Controls.Add(cbHotel);
            Controls.Add(lblStatus);
            Controls.Add(lblCategory);
            Controls.Add(lblPeople);
            Controls.Add(lblRoom);
            Controls.Add(btnEdRoom);
            Controls.Add(cbEdStatus);
            Controls.Add(cbEdCategory);
            Controls.Add(tbEdRoom);
            Name = "EditRoom";
            Text = "Добавить комнату";
            Load += EditRoom_Load;
            ((System.ComponentModel.ISupportInitialize)countHumans).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbEdRoom;
        private ComboBox cbEdCategory;
        private TextBox tbEdPeople;
        private ComboBox cbEdStatus;
        private Button btnEdRoom;
        private Label lblRoom;
        private Label lblPeople;
        private Label lblCategory;
        private Label lblStatus;
        private ComboBox cbHotel;
        private Label lblHotel;
        private NumericUpDown countHumans;
    }
}