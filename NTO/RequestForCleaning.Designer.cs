﻿namespace NTO
{
    partial class RequestForCleaning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblRegClean = new Label();
            dgvRegClean = new DataGridView();
            FIO = new DataGridViewTextBoxColumn();
            Date = new DataGridViewTextBoxColumn();
            Hotel = new DataGridViewTextBoxColumn();
            Room = new DataGridViewTextBoxColumn();
            Status = new DataGridViewTextBoxColumn();
            btnRegClean = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvRegClean).BeginInit();
            SuspendLayout();
            // 
            // lblRegClean
            // 
            lblRegClean.AutoSize = true;
            lblRegClean.Font = new Font("Segoe UI", 16F, FontStyle.Regular, GraphicsUnit.Point);
            lblRegClean.Location = new Point(226, 9);
            lblRegClean.Name = "lblRegClean";
            lblRegClean.Size = new Size(323, 30);
            lblRegClean.TabIndex = 0;
            lblRegClean.Text = "           Заявки на уборку           ";
            // 
            // dgvRegClean
            // 
            dgvRegClean.AllowUserToAddRows = false;
            dgvRegClean.AllowUserToDeleteRows = false;
            dgvRegClean.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvRegClean.Columns.AddRange(new DataGridViewColumn[] { FIO, Date, Hotel, Room, Status });
            dgvRegClean.Location = new Point(24, 48);
            dgvRegClean.Name = "dgvRegClean";
            dgvRegClean.ReadOnly = true;
            dgvRegClean.RowHeadersVisible = false;
            dgvRegClean.RowTemplate.Height = 25;
            dgvRegClean.Size = new Size(750, 325);
            dgvRegClean.TabIndex = 1;
            // 
            // FIO
            // 
            FIO.DataPropertyName = "FIO";
            FIO.HeaderText = "ФИО";
            FIO.Name = "FIO";
            FIO.ReadOnly = true;
            FIO.Width = 350;
            // 
            // Date
            // 
            Date.DataPropertyName = "Date";
            Date.HeaderText = "Дата";
            Date.Name = "Date";
            Date.ReadOnly = true;
            // 
            // Hotel
            // 
            Hotel.DataPropertyName = "HotelName";
            Hotel.HeaderText = "Отель";
            Hotel.Name = "Hotel";
            Hotel.ReadOnly = true;
            // 
            // Room
            // 
            Room.DataPropertyName = "RoomNumber";
            Room.HeaderText = "Комната";
            Room.Name = "Room";
            Room.ReadOnly = true;
            // 
            // Status
            // 
            Status.DataPropertyName = "StatusName";
            Status.HeaderText = "Статус";
            Status.Name = "Status";
            Status.ReadOnly = true;
            // 
            // btnRegClean
            // 
            btnRegClean.Location = new Point(635, 392);
            btnRegClean.Name = "btnRegClean";
            btnRegClean.Size = new Size(139, 36);
            btnRegClean.TabIndex = 2;
            btnRegClean.Text = "Оформить заявку";
            btnRegClean.UseVisualStyleBackColor = true;
            btnRegClean.Click += btnRegClean_Click;
            // 
            // RequestForCleaning
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnRegClean);
            Controls.Add(dgvRegClean);
            Controls.Add(lblRegClean);
            Name = "RequestForCleaning";
            Text = "Заявки на уборку";
            Load += RequestForCleaning_Load;
            ((System.ComponentModel.ISupportInitialize)dgvRegClean).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblRegClean;
        private DataGridView dgvRegClean;
        private Button btnRegClean;
        private DataGridViewTextBoxColumn FIO;
        private DataGridViewTextBoxColumn Date;
        private DataGridViewTextBoxColumn Hotel;
        private DataGridViewTextBoxColumn Room;
        private DataGridViewTextBoxColumn Status;
    }
}