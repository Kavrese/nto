﻿namespace NTO
{
    partial class EditDied
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditDied));
            dtpDSeason1 = new DateTimePicker();
            dtpDSeason2 = new DateTimePicker();
            lblDSeasonStart = new Label();
            lblDSeasonEnd = new Label();
            btnEditDied = new Button();
            SuspendLayout();
            // 
            // dtpDSeason1
            // 
            dtpDSeason1.Location = new Point(44, 68);
            dtpDSeason1.Name = "dtpDSeason1";
            dtpDSeason1.Size = new Size(313, 23);
            dtpDSeason1.TabIndex = 0;
            // 
            // dtpDSeason2
            // 
            dtpDSeason2.Location = new Point(439, 68);
            dtpDSeason2.Name = "dtpDSeason2";
            dtpDSeason2.Size = new Size(313, 23);
            dtpDSeason2.TabIndex = 1;
            // 
            // lblDSeasonStart
            // 
            lblDSeasonStart.AutoSize = true;
            lblDSeasonStart.BackColor = SystemColors.ControlLight;
            lblDSeasonStart.Location = new Point(44, 50);
            lblDSeasonStart.Name = "lblDSeasonStart";
            lblDSeasonStart.Size = new Size(49, 15);
            lblDSeasonStart.TabIndex = 2;
            lblDSeasonStart.Text = "Начало";
            // 
            // lblDSeasonEnd
            // 
            lblDSeasonEnd.AutoSize = true;
            lblDSeasonEnd.BackColor = SystemColors.ControlLight;
            lblDSeasonEnd.Location = new Point(439, 50);
            lblDSeasonEnd.Name = "lblDSeasonEnd";
            lblDSeasonEnd.Size = new Size(41, 15);
            lblDSeasonEnd.TabIndex = 3;
            lblDSeasonEnd.Text = "Конец";
            // 
            // btnEditDied
            // 
            btnEditDied.BackColor = SystemColors.ActiveCaption;
            btnEditDied.Location = new Point(612, 388);
            btnEditDied.Name = "btnEditDied";
            btnEditDied.Size = new Size(140, 38);
            btnEditDied.TabIndex = 4;
            btnEditDied.Text = "Сохранить изменения";
            btnEditDied.UseVisualStyleBackColor = false;
            // 
            // EditDied
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(800, 450);
            Controls.Add(btnEditDied);
            Controls.Add(lblDSeasonEnd);
            Controls.Add(lblDSeasonStart);
            Controls.Add(dtpDSeason2);
            Controls.Add(dtpDSeason1);
            Name = "EditDied";
            Text = "Изменение \"мёртвого\" сезона";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DateTimePicker dtpDSeason1;
        private DateTimePicker dtpDSeason2;
        private Label lblDSeasonStart;
        private Label lblDSeasonEnd;
        private Button btnEditDied;
    }
}