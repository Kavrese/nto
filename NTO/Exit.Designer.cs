﻿namespace NTO
{
    partial class Exit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            cbExit = new ComboBox();
            btnExit = new Button();
            label1 = new Label();
            label2 = new Label();
            SuspendLayout();
            // 
            // cbExit
            // 
            cbExit.DisplayMember = "DisplayMember";
            cbExit.DropDownStyle = ComboBoxStyle.DropDownList;
            cbExit.FormattingEnabled = true;
            cbExit.Location = new Point(12, 86);
            cbExit.Name = "cbExit";
            cbExit.Size = new Size(579, 23);
            cbExit.TabIndex = 0;
            // 
            // btnExit
            // 
            btnExit.BackColor = SystemColors.ActiveCaption;
            btnExit.Location = new Point(12, 127);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(166, 42);
            btnExit.TabIndex = 1;
            btnExit.Text = "Оформить";
            btnExit.UseVisualStyleBackColor = false;
            btnExit.Click += btnExit_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlLight;
            label1.Location = new Point(12, 54);
            label1.Name = "label1";
            label1.Size = new Size(232, 15);
            label1.TabIndex = 2;
            label1.Text = "Список броней на особождение сегодня";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(12, 9);
            label2.Name = "label2";
            label2.Size = new Size(207, 32);
            label2.TabIndex = 5;
            label2.Text = "К освобождению";
            // 
            // Exit
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(603, 180);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btnExit);
            Controls.Add(cbExit);
            Name = "Exit";
            Text = "Выезд";
            Load += Exit_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbExit;
        private Button btnExit;
        private Label label1;
        private Label label2;
    }
}