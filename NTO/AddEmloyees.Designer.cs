﻿namespace NTO
{
    partial class AddEmloyees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEmloyees));
            tbAEmp = new TextBox();
            tbAEmp2 = new TextBox();
            tbAEmp3 = new TextBox();
            cbCatAddEmp = new ComboBox();
            btnAEmp = new Button();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            SuspendLayout();
            // 
            // tbAEmp
            // 
            tbAEmp.Location = new Point(46, 40);
            tbAEmp.Name = "tbAEmp";
            tbAEmp.Size = new Size(270, 23);
            tbAEmp.TabIndex = 0;
            // 
            // tbAEmp2
            // 
            tbAEmp2.Location = new Point(46, 95);
            tbAEmp2.Name = "tbAEmp2";
            tbAEmp2.Size = new Size(270, 23);
            tbAEmp2.TabIndex = 1;
            // 
            // tbAEmp3
            // 
            tbAEmp3.Location = new Point(46, 149);
            tbAEmp3.Name = "tbAEmp3";
            tbAEmp3.Size = new Size(270, 23);
            tbAEmp3.TabIndex = 2;
            // 
            // cbCatAddEmp
            // 
            cbCatAddEmp.DisplayMember = "Name";
            cbCatAddEmp.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCatAddEmp.FormattingEnabled = true;
            cbCatAddEmp.Location = new Point(413, 40);
            cbCatAddEmp.Name = "cbCatAddEmp";
            cbCatAddEmp.Size = new Size(270, 23);
            cbCatAddEmp.TabIndex = 3;
            // 
            // btnAEmp
            // 
            btnAEmp.Location = new Point(46, 210);
            btnAEmp.Name = "btnAEmp";
            btnAEmp.Size = new Size(637, 38);
            btnAEmp.TabIndex = 4;
            btnAEmp.Text = "Добавить";
            btnAEmp.UseVisualStyleBackColor = true;
            btnAEmp.Click += btnAEmp_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlLightLight;
            label1.Location = new Point(46, 22);
            label1.Name = "label1";
            label1.Size = new Size(58, 15);
            label1.TabIndex = 5;
            label1.Text = "Фамилия";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ControlLightLight;
            label2.Location = new Point(46, 77);
            label2.Name = "label2";
            label2.Size = new Size(31, 15);
            label2.TabIndex = 6;
            label2.Text = "Имя";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = SystemColors.ControlLightLight;
            label3.Location = new Point(46, 131);
            label3.Name = "label3";
            label3.Size = new Size(58, 15);
            label3.TabIndex = 7;
            label3.Text = "Отчество";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = SystemColors.ControlLightLight;
            label4.Location = new Point(413, 22);
            label4.Name = "label4";
            label4.Size = new Size(63, 15);
            label4.TabIndex = 8;
            label4.Text = "Категория";
            // 
            // AddEmloyees
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(708, 260);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btnAEmp);
            Controls.Add(cbCatAddEmp);
            Controls.Add(tbAEmp3);
            Controls.Add(tbAEmp2);
            Controls.Add(tbAEmp);
            Name = "AddEmloyees";
            Text = "Добавление сотрудника";
            Load += AddEmloyees_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbAEmp;
        private TextBox tbAEmp2;
        private TextBox tbAEmp3;
        private ComboBox cbCatAddEmp;
        private Button btnAEmp;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
    }
}