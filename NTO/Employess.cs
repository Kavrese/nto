﻿using NTO.database;

namespace NTO
{
    public partial class Employess : Form
    {
        public Employess()
        {
            InitializeComponent();
        }

        private void Employess_Load(object sender, EventArgs e)
        {
            dgvEmp.AutoGenerateColumns = false;
            using (Context context = new())
            {
                dgvEmp.DataSource = context.GetEmployees();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddEmloyees form = new AddEmloyees();
            form.Closed += Closed;
            form.Show();
        }

        public void Closed(object sender, EventArgs e) 
        {
            using (Context context = new())
            {
                dgvEmp.DataSource = context.GetEmployees();
            }
        }
    }
}
