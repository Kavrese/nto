﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class Categories : Form
    {
        public Categories()
        {
            InitializeComponent();
        }

        List<ModelCategory> allCategories;
        List<ModelHotel> hotels;

        private void Categories_Load_1(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                allCategories = context.GetCategories();
                hotels = context.GetHotels();

                dgvCategories.Columns.Add("Категория/Отель:", "Категория/Отель:");

                for (int i = 0; i < hotels.Count; i++)
                {
                    dgvCategories.Columns.Add(hotels[i].Name, hotels[i].Name);
                }

                for (int i = 0; i < 3; i++)
                {
                    dgvCategories.Rows.Add();

                    dgvCategories.Rows[i].Cells[0].Value = new List<String>() { "Стандарт", "Люкс", "Апартамент" }[i];
                }

                for (int i = 0; i < hotels.Count; i++)
                {
                    string firstColumnValue1 = (string)dgvCategories.Rows[0].Cells[0].Value;
                    string firstColumnValue2 = (string)dgvCategories.Rows[1].Cells[0].Value;
                    string firstColumnValue3 = (string)dgvCategories.Rows[2].Cells[0].Value;

                    var a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue1);
                    dgvCategories.Rows[0].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";

                    a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue2);
                    dgvCategories.Rows[1].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";

                    a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue3);
                    dgvCategories.Rows[2].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Report().Show();
        }

        private void btnCategories_Click(object sender, EventArgs e)
        {
            var cell = dgvCategories.SelectedCells[0];
            if (cell.ColumnIndex != 0)
            {
                var indexHotel = cell.ColumnIndex - 1;
                var indexCategory = cell.RowIndex;
                var hotel = hotels[indexHotel];
                var category = allCategories.Where(t => t.HotelId == hotel.Id && t.Name == (string)dgvCategories.Rows[indexCategory].Cells[0].Value).ToList()[0];

                var form = new EditCategories(category, hotel);
                form.Closed += Closed;
                form.Show();
            }

        }

        public void Closed(object sender, EventArgs e)
        {
            dgvCategories.Rows.Clear();
            using (Context context = new())
            {
                allCategories = context.GetCategories();
                hotels = context.GetHotels();

                dgvCategories.Columns.Add("Категория/Отель:", "Категория/Отель:");

                for (int i = 0; i < hotels.Count; i++)
                {
                    dgvCategories.Columns.Add(hotels[i].Name, hotels[i].Name);
                }

                for (int i = 0; i < 3; i++)
                {
                    dgvCategories.Rows.Add();

                    dgvCategories.Rows[i].Cells[0].Value = new List<String>() { "Стандарт", "Люкс", "Апартамент" }[i];
                }

                for (int i = 0; i < hotels.Count; i++)
                {
                    string firstColumnValue1 = (string)dgvCategories.Rows[0].Cells[0].Value;
                    string firstColumnValue2 = (string)dgvCategories.Rows[1].Cells[0].Value;
                    string firstColumnValue3 = (string)dgvCategories.Rows[2].Cells[0].Value;

                    var a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue1);
                    dgvCategories.Rows[0].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";

                    a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue2);
                    dgvCategories.Rows[1].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";

                    a = allCategories.Where(t => t.HotelId == hotels[i].Id && t.Name == firstColumnValue3);
                    dgvCategories.Rows[2].Cells[i + 1].Value = a.First().Price.ToString() + "р./cут";
                }
            }
        }
    }
}
