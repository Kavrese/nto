﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class AddEmloyees : Form
    {
        public AddEmloyees()
        {
            InitializeComponent();
        }

        private void btnAEmp_Click(object sender, EventArgs e)
        {
            var model = new ModelEmployee();
            model.Lastname = tbAEmp.Text;
            model.Firstname = tbAEmp2.Text;
            model.Patronymic = tbAEmp3.Text;
            model.CategoryId = (cbCatAddEmp.SelectedItem as ModelCategoryEmployee).Id;
            using (Context context = new())
            {
                context.Employees.Add(model);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }
        }

        private void AddEmloyees_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                cbCatAddEmp.DataSource = context.CategoriesEmployees.ToList();
            }
        }
    }
}
