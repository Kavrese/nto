﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class AddClients : Form
    {
        public AddClients()
        {
            InitializeComponent();
        }

        private void AddClients_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                cbClients.DataSource = context.Guests.ToList();
            }
        }

        private void btnAddClients_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                var client = new ModelClient();
                client.GuestId = (cbClients.SelectedItem as ModelGuest).Id;
                client.isPhysics = !checkBoxYrFace.Checked;
                context.Add(client);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }
        }
    }
}
