﻿using NTO.database;
using NTO.database.models;
using System.Data;

namespace NTO
{
    public partial class Hotel : Form
    {
        List<ModelHotel> hotels;

        public Hotel()
        {
            InitializeComponent();
        }

        private void Hotel_Load(object sender, EventArgs e)
        {
            dgvHotel.AutoGenerateColumns = false;
            using (Context context = new())
            {
                hotels = context.GetHotels();
                dgvHotel.DataSource = hotels;
            }
        }

        private void btnRoom_Click(object sender, EventArgs e)
        {
            var hotel = hotels.Where(t => t.Name == dgvHotel.SelectedRows[0].Cells[0].Value.ToString()).Single();
            var form = new Room(hotel.Id);
            form.Show();
        }
    }
}
