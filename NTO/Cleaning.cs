﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;
using System;
namespace NTO
{
    public partial class Cleaning : Form
    {
        public Cleaning()
        {
            InitializeComponent();
        }

        List<ModelRoom> rooms;

        private void dgvCleaning_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Cleaning_Load(object sender, EventArgs e)
        {
            dgvCleaning.AutoGenerateColumns = false;
            using (Context context = new())
            {
                var alreadyExistRoomsIds = context.GetCleanActions().Select(t => t.Room.Id).ToList();
                rooms = context.Rooms
                    .Include(t => t.Status)
                    .Where(t => t.Status.Name == "Грязный и свободный" || t.Status.Name == "Грязный и занят")
                    .Where(t => !alreadyExistRoomsIds.Contains(t.Id))
                    .ToList();
                dgvCleaning.DataSource = rooms;
                comboBoxEmployee.DataSource = context.GetEmployees();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                var empId = (comboBoxEmployee.SelectedItem as ModelEmployee).Id;
                var roomNumber = dgvCleaning.SelectedCells[0].Value as string;
                var roomId = rooms.Where(t => t.Number == roomNumber).Single().Id;
                var model = new ModelCleanAction() {
                    Id = Guid.NewGuid(), Date = DateTime.Now,
                    EmployeeId = empId, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                    RoomId = roomId, StatusId = Guid.Parse("591006ab-ada0-4d9c-8dae-579c8ebf097a")
                };
                context.CleanActions.Add(model);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }
        }
    }
}
