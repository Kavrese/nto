﻿using Microsoft.EntityFrameworkCore;
using NTO.database;

namespace NTO
{
    public partial class Booking : Form
    {
        public Booking()
        {
            InitializeComponent();
        }

        private void Booking_Load(object sender, EventArgs e)
        {
            dgvBooking.AutoGenerateColumns = false;
            using (Context context = new())
            {
                dgvBooking.DataSource = context.GetBookingList();
            }
        }

        private void btnBooking_Click(object sender, EventArgs e)
        {
            var form = new AddBooking();
            form.Closed += Closed;
            form.Show();
        }

        private void Closed(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                dgvBooking.DataSource = context.GetBookingList();
            }
        }
    }
}
