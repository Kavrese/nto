﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;
using System.Data;

namespace NTO
{
    public partial class Exit : Form
    {
        public Exit()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ModelBooking model = cbExit.SelectedItem as ModelBooking;
            using (Context context = new())
            {
                var room = context.Rooms.Where(t => t.Id == model.RoomId).Single();
                room.StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf15"); // Грязный и свободный

                context.Entry(model).State = EntityState.Modified;

                model.isCompleteExit = true;
                context.SaveChanges();
            }

            MessageBox.Show($"Бронь снята, статус '{model.Room.Number}' комнаты теперь 'Грязный и свободный'");
        }

        private void Exit_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                cbExit.DataSource = context.GetBookingsForExitAction();
            }
        }
    }
}
