﻿namespace NTO.database.models
{
    public class ModelCleanAction
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }

        public Guid EmployeeId { get; set; }
        public ModelEmployee Employee { get; set; }

        public Guid HotelId { get; set; }

        public ModelHotel Hotel { get; set; }

        public Guid RoomId { get; set; }

        public ModelRoom Room { get; set; }

        public Guid StatusId { get; set; }

        public ModelStatusCleaning Status { get; set; }

        public string RoomNumber { get { return Room.Number; } }

        public string FIO { get { return Employee.FIO; } }
        public string HotelName { get { return Hotel.Name; } }
        public string StatusName { get { return Status.Name; } }
    }
}
