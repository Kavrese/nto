﻿
namespace NTO.database.models
{
    public class ModelRegion
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
