﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTO.database.models
{
    internal class ListGuests
    {
        public Guid Id { get; set; }

        public Guid ActionId { get; set; }

        public Guid GuestId { get; set; }

        public ModelGuest Guest { get; set; }

        public ModelAction Action { get; set; }
    }
}
