﻿namespace NTO.database.models
{
    public class ModelStatusCleaning
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
