﻿namespace NTO.database.models
{
    public class ModelCategoryHotel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Sale { get; set; }
    }
}
