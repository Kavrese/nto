﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    public class ModelHotel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid RegionId { get; set; }

        public ModelRegion Region { get; set; }

        public Guid CategoryHotelId { get; set; }

        public ModelCategoryHotel CategoryHotel { get; set; }

        [NotMapped]
        public string NameRegion { get { return Region.Name; } }

        [NotMapped]
        public string NameWithCategoryHotel { get { return $"{CategoryHotel.Name} - {Name}"; } }

        [NotMapped]
        public string CategoryName { get { return CategoryHotel.Name; } }

    }
}
