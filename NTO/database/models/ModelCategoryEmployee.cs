﻿namespace NTO.database.models
{
    public class ModelCategoryEmployee
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
