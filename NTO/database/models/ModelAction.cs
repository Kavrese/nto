﻿using Microsoft.EntityFrameworkCore;

namespace NTO.database.models
{
    internal class ModelAction
    {
        public Guid Id { get; set; }
        public ModelHotel Hotel { get; set; }
        public ModelRoom Room { get; set; }
        public ModelBooking Booking { get; set; }
        public DateOnly EndDate { get; set; }
        public Guid BookingId { get; set; }
        public Guid HotelId { get; set; }
        public Guid RoomId { get; set; }

        public List<ModelGuest> GetExitGuests(Context context)
        {
            return context.ListGuests
                .Where(t => t.ActionId == Id)
                .Include(t => t.Guest)
                .Select(t => context.Guests.Where(r => r.Id == t.GuestId).Single())
                .ToList();
        }

        public string FIO { get { return Booking.Client.FIO; } }

        public bool isCompleteEnter { get; set; } = false;
        public bool isCompleteExit { get; set; } = false;
    }
}
