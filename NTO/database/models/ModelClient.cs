﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    internal class ModelClient
    {
        public Guid Id { get; set; }

        public Guid GuestId { get; set; }

        public ModelGuest Guest { get; set; }

        public bool isPhysics { get; set; }

        public string Status { get {
                if (isPhysics)
                {
                    return "Физ. лицо";
                }
                else {
                    return "Юр. лицо";
                }
        }}


        [NotMapped]
        public string FIO { get { return Guest.FIO; } }
        [NotMapped]
        public string Firstname { get { return Guest.Firstname; } }
        [NotMapped]
        public string Lastname { get { return Guest.Lastname; } }
        [NotMapped]
        public string Patronymic { get { return Guest.Patronymic; } }
        [NotMapped]
        public string Phone { get { return Guest.Phone; } }
    }
}
