﻿namespace NTO.database.models
{
    public class ModelEmployee
    {
        public Guid Id { get; set; }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Patronymic { get; set; }
        public ModelCategoryEmployee Category { get; set; }
        public Guid CategoryId { get; set; }

        public string FIO { get { return $"{Lastname} {Firstname[0]}.{Patronymic[0]}"; } }

        public string CategoryName { get { return Category.Name; } }
    }
}
