﻿namespace NTO.database.models
{
    public class ModelStatus
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
