﻿namespace NTO
{
    partial class NormalRep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnNRep = new Button();
            dateTimePickerEnd = new DateTimePicker();
            comboBoxHotels = new ComboBox();
            label1 = new Label();
            label2 = new Label();
            reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            panelNorRep = new Panel();
            panelNorRep.SuspendLayout();
            SuspendLayout();
            // 
            // btnNRep
            // 
            btnNRep.Location = new Point(620, 12);
            btnNRep.Name = "btnNRep";
            btnNRep.Size = new Size(168, 38);
            btnNRep.TabIndex = 1;
            btnNRep.Text = "Создать отчёт";
            btnNRep.UseVisualStyleBackColor = true;
            btnNRep.Click += btnNRep_Click;
            // 
            // dateTimePickerEnd
            // 
            dateTimePickerEnd.CalendarFont = new Font("Segoe UI", 8F, FontStyle.Regular, GraphicsUnit.Point);
            dateTimePickerEnd.Location = new Point(1, 27);
            dateTimePickerEnd.Margin = new Padding(3, 2, 3, 2);
            dateTimePickerEnd.Name = "dateTimePickerEnd";
            dateTimePickerEnd.Size = new Size(219, 23);
            dateTimePickerEnd.TabIndex = 3;
            // 
            // comboBoxHotels
            // 
            comboBoxHotels.DisplayMember = "Name";
            comboBoxHotels.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxHotels.FormattingEnabled = true;
            comboBoxHotels.Location = new Point(226, 27);
            comboBoxHotels.Name = "comboBoxHotels";
            comboBoxHotels.Size = new Size(388, 23);
            comboBoxHotels.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(1, 9);
            label1.Name = "label1";
            label1.Size = new Size(32, 15);
            label1.TabIndex = 5;
            label1.Text = "Дата";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(226, 9);
            label2.Name = "label2";
            label2.Size = new Size(40, 15);
            label2.TabIndex = 6;
            label2.Text = "Отель";
            // 
            // reportViewer1
            // 
            reportViewer1.Dock = DockStyle.Fill;
            reportViewer1.Location = new Point(0, 0);
            reportViewer1.Name = "ReportViewer";
            reportViewer1.ServerReport.BearerToken = null;
            reportViewer1.Size = new Size(800, 378);
            reportViewer1.TabIndex = 0;
            // 
            // panelNorRep
            // 
            panelNorRep.Controls.Add(reportViewer1);
            panelNorRep.Dock = DockStyle.Bottom;
            panelNorRep.Location = new Point(0, 72);
            panelNorRep.Name = "panelNorRep";
            panelNorRep.Size = new Size(800, 378);
            panelNorRep.TabIndex = 0;
            panelNorRep.Paint += panelNorRep_Paint;
            // 
            // NormalRep
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(comboBoxHotels);
            Controls.Add(dateTimePickerEnd);
            Controls.Add(btnNRep);
            Controls.Add(panelNorRep);
            Name = "NormalRep";
            Text = "Отчёт";
            Load += NormalRep_Load;
            panelNorRep.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Button btnNRep;
        private DateTimePicker dateTimePickerEnd;
        private ComboBox comboBoxHotels;
        private Label label1;
        private Label label2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Panel panelNorRep;
    }
}