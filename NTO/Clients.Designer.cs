﻿namespace NTO
{
    partial class Clients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clients));
            dgvClients = new DataGridView();
            LastName = new DataGridViewTextBoxColumn();
            FirstName = new DataGridViewTextBoxColumn();
            PatronymicName = new DataGridViewTextBoxColumn();
            Status = new DataGridViewTextBoxColumn();
            Phone = new DataGridViewTextBoxColumn();
            btnClients = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvClients).BeginInit();
            SuspendLayout();
            // 
            // dgvClients
            // 
            dgvClients.AllowUserToAddRows = false;
            dgvClients.AllowUserToDeleteRows = false;
            dgvClients.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvClients.Columns.AddRange(new DataGridViewColumn[] { LastName, FirstName, PatronymicName, Status, Phone });
            dgvClients.Location = new Point(1, 0);
            dgvClients.Name = "dgvClients";
            dgvClients.ReadOnly = true;
            dgvClients.RowHeadersWidth = 51;
            dgvClients.RowTemplate.Height = 25;
            dgvClients.Size = new Size(710, 306);
            dgvClients.TabIndex = 0;
            // 
            // LastName
            // 
            LastName.DataPropertyName = "Lastname";
            LastName.HeaderText = "Фамилия";
            LastName.MinimumWidth = 6;
            LastName.Name = "LastName";
            LastName.ReadOnly = true;
            LastName.Width = 125;
            // 
            // FirstName
            // 
            FirstName.DataPropertyName = "Firstname";
            FirstName.HeaderText = "Имя";
            FirstName.MinimumWidth = 6;
            FirstName.Name = "FirstName";
            FirstName.ReadOnly = true;
            FirstName.Width = 125;
            // 
            // PatronymicName
            // 
            PatronymicName.DataPropertyName = "Patronymic";
            PatronymicName.HeaderText = "Отчество";
            PatronymicName.MinimumWidth = 6;
            PatronymicName.Name = "PatronymicName";
            PatronymicName.ReadOnly = true;
            PatronymicName.Width = 125;
            // 
            // Status
            // 
            Status.DataPropertyName = "Status";
            Status.HeaderText = "Статус";
            Status.MinimumWidth = 6;
            Status.Name = "Status";
            Status.ReadOnly = true;
            Status.Width = 125;
            // 
            // Phone
            // 
            Phone.DataPropertyName = "Phone";
            Phone.HeaderText = "Номер телефона";
            Phone.MinimumWidth = 6;
            Phone.Name = "Phone";
            Phone.ReadOnly = true;
            Phone.Width = 125;
            // 
            // btnClients
            // 
            btnClients.BackColor = SystemColors.ActiveCaption;
            btnClients.Location = new Point(570, 321);
            btnClients.Name = "btnClients";
            btnClients.Size = new Size(140, 42);
            btnClients.TabIndex = 1;
            btnClients.Text = "Добавить";
            btnClients.UseVisualStyleBackColor = false;
            btnClients.Click += btnClients_Click;
            // 
            // Clients
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(716, 368);
            Controls.Add(btnClients);
            Controls.Add(dgvClients);
            Name = "Clients";
            Text = "Клиенты";
            Load += Clients_Load;
            ((System.ComponentModel.ISupportInitialize)dgvClients).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvClients;
        private Button btnClients;
        private DataGridViewTextBoxColumn LastName;
        private DataGridViewTextBoxColumn FirstName;
        private DataGridViewTextBoxColumn PatronymicName;
        private DataGridViewTextBoxColumn Status;
        private DataGridViewTextBoxColumn Phone;
    }
}