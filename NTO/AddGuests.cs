﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class AddGuests : Form
    {
        public AddGuests()
        {
            InitializeComponent();
        }

        private void btnAddGuests_Click(object sender, EventArgs e)
        {
            var model = new ModelGuest();
            model.Firstname = tbGFN.Text;
            model.Lastname = tbGLN.Text;
            model.Patronymic = tbGPN.Text;
            model.Phone = mtbGPhone.Text;

            using (Context context = new())
            {
                context.Guests.Add(model);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }
        }
    }
}
