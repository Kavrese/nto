﻿namespace NTO
{
    partial class Booking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Booking));
            dgvBooking = new DataGridView();
            Hotel = new DataGridViewTextBoxColumn();
            StartDate = new DataGridViewTextBoxColumn();
            EndDate = new DataGridViewTextBoxColumn();
            Cost = new DataGridViewTextBoxColumn();
            NumberRoom = new DataGridViewTextBoxColumn();
            LFPName = new DataGridViewTextBoxColumn();
            btnBooking = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvBooking).BeginInit();
            SuspendLayout();
            // 
            // dgvBooking
            // 
            dgvBooking.AllowUserToAddRows = false;
            dgvBooking.AllowUserToDeleteRows = false;
            dgvBooking.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvBooking.Columns.AddRange(new DataGridViewColumn[] { Hotel, StartDate, EndDate, Cost, NumberRoom, LFPName });
            dgvBooking.Location = new Point(10, 10);
            dgvBooking.Name = "dgvBooking";
            dgvBooking.ReadOnly = true;
            dgvBooking.RowHeadersWidth = 51;
            dgvBooking.RowTemplate.Height = 25;
            dgvBooking.Size = new Size(709, 342);
            dgvBooking.TabIndex = 0;
            // 
            // Hotel
            // 
            Hotel.DataPropertyName = "HotelName";
            Hotel.HeaderText = "Отель";
            Hotel.MinimumWidth = 6;
            Hotel.Name = "Hotel";
            Hotel.ReadOnly = true;
            Hotel.Width = 125;
            // 
            // StartDate
            // 
            StartDate.DataPropertyName = "startDate";
            StartDate.HeaderText = "Дата заезда";
            StartDate.MinimumWidth = 6;
            StartDate.Name = "StartDate";
            StartDate.ReadOnly = true;
            StartDate.Width = 125;
            // 
            // EndDate
            // 
            EndDate.DataPropertyName = "endDate";
            EndDate.HeaderText = "Дата отъезда";
            EndDate.MinimumWidth = 6;
            EndDate.Name = "EndDate";
            EndDate.ReadOnly = true;
            EndDate.Width = 125;
            // 
            // Cost
            // 
            Cost.DataPropertyName = "Cost";
            Cost.HeaderText = "Стоимость";
            Cost.MinimumWidth = 6;
            Cost.Name = "Cost";
            Cost.ReadOnly = true;
            Cost.Width = 125;
            // 
            // NumberRoom
            // 
            NumberRoom.DataPropertyName = "Number";
            NumberRoom.HeaderText = "Номер комнаты";
            NumberRoom.MinimumWidth = 6;
            NumberRoom.Name = "NumberRoom";
            NumberRoom.ReadOnly = true;
            NumberRoom.Width = 125;
            // 
            // LFPName
            // 
            LFPName.DataPropertyName = "FIO";
            LFPName.HeaderText = "ФИО";
            LFPName.MinimumWidth = 6;
            LFPName.Name = "LFPName";
            LFPName.ReadOnly = true;
            LFPName.Width = 125;
            // 
            // btnBooking
            // 
            btnBooking.BackColor = SystemColors.ActiveCaption;
            btnBooking.Location = new Point(570, 364);
            btnBooking.Name = "btnBooking";
            btnBooking.Size = new Size(150, 43);
            btnBooking.TabIndex = 1;
            btnBooking.Text = "Добавить";
            btnBooking.UseVisualStyleBackColor = false;
            btnBooking.Click += btnBooking_Click;
            // 
            // Booking
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(745, 416);
            Controls.Add(btnBooking);
            Controls.Add(dgvBooking);
            Name = "Booking";
            Text = "Бронирование";
            Load += Booking_Load;
            ((System.ComponentModel.ISupportInitialize)dgvBooking).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvBooking;
        private Button btnBooking;
        private DataGridViewTextBoxColumn Hotel;
        private DataGridViewTextBoxColumn StartDate;
        private DataGridViewTextBoxColumn EndDate;
        private DataGridViewTextBoxColumn Cost;
        private DataGridViewTextBoxColumn NumberRoom;
        private DataGridViewTextBoxColumn LFPName;
    }
}