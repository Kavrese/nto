﻿namespace NTO
{
    partial class Guests
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Guests));
            dgvGuests = new DataGridView();
            LastName = new DataGridViewTextBoxColumn();
            FirstName = new DataGridViewTextBoxColumn();
            PatronymicName = new DataGridViewTextBoxColumn();
            Phone = new DataGridViewTextBoxColumn();
            btnGuests = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvGuests).BeginInit();
            SuspendLayout();
            // 
            // dgvGuests
            // 
            dgvGuests.AllowUserToAddRows = false;
            dgvGuests.AllowUserToDeleteRows = false;
            dgvGuests.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvGuests.Columns.AddRange(new DataGridViewColumn[] { LastName, FirstName, PatronymicName, Phone });
            dgvGuests.Location = new Point(0, 0);
            dgvGuests.Name = "dgvGuests";
            dgvGuests.ReadOnly = true;
            dgvGuests.RowHeadersVisible = false;
            dgvGuests.RowHeadersWidth = 51;
            dgvGuests.RowTemplate.Height = 25;
            dgvGuests.Size = new Size(694, 326);
            dgvGuests.TabIndex = 0;
            // 
            // LastName
            // 
            LastName.DataPropertyName = "Lastname";
            LastName.HeaderText = "Фамилия";
            LastName.MinimumWidth = 6;
            LastName.Name = "LastName";
            LastName.ReadOnly = true;
            LastName.Width = 150;
            // 
            // FirstName
            // 
            FirstName.DataPropertyName = "Firstname";
            FirstName.HeaderText = "Имя";
            FirstName.MinimumWidth = 6;
            FirstName.Name = "FirstName";
            FirstName.ReadOnly = true;
            FirstName.Width = 150;
            // 
            // PatronymicName
            // 
            PatronymicName.DataPropertyName = "Patronymic";
            PatronymicName.HeaderText = "Отчество";
            PatronymicName.MinimumWidth = 6;
            PatronymicName.Name = "PatronymicName";
            PatronymicName.ReadOnly = true;
            PatronymicName.Width = 150;
            // 
            // Phone
            // 
            Phone.DataPropertyName = "Phone";
            Phone.HeaderText = "Номер телефона";
            Phone.MinimumWidth = 6;
            Phone.Name = "Phone";
            Phone.ReadOnly = true;
            Phone.Width = 300;
            // 
            // btnGuests
            // 
            btnGuests.BackColor = SystemColors.ActiveCaption;
            btnGuests.Location = new Point(535, 340);
            btnGuests.Name = "btnGuests";
            btnGuests.Size = new Size(159, 41);
            btnGuests.TabIndex = 1;
            btnGuests.Text = "Добавить";
            btnGuests.UseVisualStyleBackColor = false;
            btnGuests.Click += btnGuests_Click;
            // 
            // Guests
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(698, 388);
            Controls.Add(btnGuests);
            Controls.Add(dgvGuests);
            Name = "Guests";
            Text = "Гости";
            Load += Guests_Load;
            ((System.ComponentModel.ISupportInitialize)dgvGuests).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvGuests;
        private Button btnGuests;
        private DataGridViewTextBoxColumn LastName;
        private DataGridViewTextBoxColumn FirstName;
        private DataGridViewTextBoxColumn PatronymicName;
        private DataGridViewTextBoxColumn Phone;
    }
}