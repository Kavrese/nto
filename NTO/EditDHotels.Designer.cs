﻿namespace NTO
{
    partial class EditDHotels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditDHotels));
            cbEdDHotels = new ComboBox();
            cbEdDHotels2 = new ComboBox();
            btnEdDHotels = new Button();
            label1 = new Label();
            label2 = new Label();
            SuspendLayout();
            // 
            // cbEdDHotels
            // 
            cbEdDHotels.DisplayMember = "Name";
            cbEdDHotels.DropDownStyle = ComboBoxStyle.DropDownList;
            cbEdDHotels.FormattingEnabled = true;
            cbEdDHotels.Location = new Point(12, 31);
            cbEdDHotels.Name = "cbEdDHotels";
            cbEdDHotels.Size = new Size(412, 23);
            cbEdDHotels.TabIndex = 0;
            // 
            // cbEdDHotels2
            // 
            cbEdDHotels2.DisplayMember = "Name";
            cbEdDHotels2.DropDownStyle = ComboBoxStyle.DropDownList;
            cbEdDHotels2.FormattingEnabled = true;
            cbEdDHotels2.Location = new Point(13, 86);
            cbEdDHotels2.Name = "cbEdDHotels2";
            cbEdDHotels2.Size = new Size(411, 23);
            cbEdDHotels2.TabIndex = 1;
            // 
            // btnEdDHotels
            // 
            btnEdDHotels.BackColor = SystemColors.ActiveCaption;
            btnEdDHotels.Location = new Point(246, 134);
            btnEdDHotels.Name = "btnEdDHotels";
            btnEdDHotels.Size = new Size(178, 32);
            btnEdDHotels.TabIndex = 2;
            btnEdDHotels.Text = "Сохранить изменения";
            btnEdDHotels.UseVisualStyleBackColor = false;
            btnEdDHotels.Click += btnEdDHotels_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlLight;
            label1.Location = new Point(13, 13);
            label1.Name = "label1";
            label1.Size = new Size(93, 15);
            label1.TabIndex = 3;
            label1.Text = "Название отеля";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ControlLight;
            label2.Location = new Point(13, 68);
            label2.Name = "label2";
            label2.Size = new Size(97, 15);
            label2.TabIndex = 4;
            label2.Text = "Категория отеля";
            // 
            // EditDHotels
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(438, 177);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btnEdDHotels);
            Controls.Add(cbEdDHotels2);
            Controls.Add(cbEdDHotels);
            Name = "EditDHotels";
            Text = "Изменение направления отелей";
            Load += EditDHotels_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbEdDHotels;
        private ComboBox cbEdDHotels2;
        private Button btnEdDHotels;
        private Label label1;
        private Label label2;
    }
}