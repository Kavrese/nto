﻿namespace NTO
{
    partial class Cleaning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            lblClean = new Label();
            txtHotel = new Label();
            dgvCleaning = new DataGridView();
            Room = new DataGridViewTextBoxColumn();
            comboBoxEmployee = new ComboBox();
            label1 = new Label();
            button1 = new Button();
            label2 = new Label();
            ((System.ComponentModel.ISupportInitialize)dgvCleaning).BeginInit();
            SuspendLayout();
            // 
            // lblClean
            // 
            lblClean.AutoSize = true;
            lblClean.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            lblClean.Location = new Point(12, 9);
            lblClean.Name = "lblClean";
            lblClean.Size = new Size(114, 32);
            lblClean.TabIndex = 0;
            lblClean.Text = "К уборке";
            // 
            // txtHotel
            // 
            txtHotel.AutoSize = true;
            txtHotel.Font = new Font("Segoe UI Semibold", 12F, FontStyle.Bold, GraphicsUnit.Point);
            txtHotel.Location = new Point(137, 58);
            txtHotel.Name = "txtHotel";
            txtHotel.Size = new Size(55, 21);
            txtHotel.TabIndex = 1;
            txtHotel.Text = "Отель";
            // 
            // dgvCleaning
            // 
            dgvCleaning.AllowUserToAddRows = false;
            dgvCleaning.AllowUserToDeleteRows = false;
            dgvCleaning.BackgroundColor = SystemColors.Control;
            dgvCleaning.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvCleaning.Columns.AddRange(new DataGridViewColumn[] { Room });
            dgvCleaning.GridColor = SystemColors.ButtonShadow;
            dgvCleaning.Location = new Point(12, 59);
            dgvCleaning.Name = "dgvCleaning";
            dgvCleaning.ReadOnly = true;
            dgvCleaning.RowHeadersVisible = false;
            dgvCleaning.RowTemplate.Height = 25;
            dgvCleaning.Size = new Size(104, 325);
            dgvCleaning.TabIndex = 2;
            dgvCleaning.CellDoubleClick += dgvCleaning_CellDoubleClick;
            // 
            // Room
            // 
            Room.DataPropertyName = "Number";
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.TopCenter;
            Room.DefaultCellStyle = dataGridViewCellStyle1;
            Room.HeaderText = "Номер к уборке";
            Room.Name = "Room";
            Room.ReadOnly = true;
            // 
            // comboBoxEmployee
            // 
            comboBoxEmployee.DisplayMember = "FIO";
            comboBoxEmployee.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxEmployee.FormattingEnabled = true;
            comboBoxEmployee.Location = new Point(233, 97);
            comboBoxEmployee.Name = "comboBoxEmployee";
            comboBoxEmployee.Size = new Size(182, 23);
            comboBoxEmployee.TabIndex = 5;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI Semibold", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(137, 97);
            label1.Name = "label1";
            label1.Size = new Size(90, 21);
            label1.TabIndex = 4;
            label1.Text = "Сотрудник";
            // 
            // button1
            // 
            button1.Location = new Point(137, 344);
            button1.Name = "button1";
            button1.Size = new Size(278, 40);
            button1.TabIndex = 6;
            button1.Text = "Сохранить";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(233, 58);
            label2.Name = "label2";
            label2.Size = new Size(146, 21);
            label2.TabIndex = 7;
            label2.Text = "Весенние ласточки";
            // 
            // Cleaning
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(435, 398);
            Controls.Add(label2);
            Controls.Add(button1);
            Controls.Add(comboBoxEmployee);
            Controls.Add(label1);
            Controls.Add(dgvCleaning);
            Controls.Add(txtHotel);
            Controls.Add(lblClean);
            Name = "Cleaning";
            Text = "Уборка";
            Load += Cleaning_Load;
            ((System.ComponentModel.ISupportInitialize)dgvCleaning).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblClean;
        private Label txtHotel;
        private DataGridView dgvCleaning;
        private ComboBox comboBoxEmployee;
        private Label label1;
        private Button button1;
        private Label label2;
        private DataGridViewTextBoxColumn Room;
    }
}