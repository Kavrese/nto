﻿using Microsoft.Reporting.WinForms;
using System.Data;

namespace NTO
{
    public partial class Report : Form
    {
        public Report()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.ReportEmbeddedResource = "NTO.Price.rdlc";
            ReportDataSource rds = new ReportDataSource("DataTable1", new DataTable());
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.RefreshReport();
        }
    }
}
